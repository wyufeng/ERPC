ERPC（Embedded Remote Procedure Call）是**一个简单的、易用的、高效的远程调用框架**。

ERPC以简化Linux应用设计为目标，当前具备如下特点：
1. 整个框架采用纯C语言实现，可很好的应用于嵌入式Linux平台中；
2. 整个框架基于POSIX标准变现，可实现跨硬件平台的多场景、多行业的应用；
3. 底层采用TCP长链接方式实现，连接稳定，通信效率高；
4. 通信采用json-rpc协议，具有很好的可视性、可扩展性，并能无缝的与云平台对接；
5. 通信数据可加密，可使用标准的SSL/TSL加密，也可自定义加密算法，满足通用和个性化数据安全的需求；
6. 框架实现了远程调用方法，将跨线程、跨进程，甚至跨CPU、跨云服务器的调用过程简化成本地调用过程；
7. 框架实现了观察者模式，可实现硬件或模块的状态监控、消息通知，配合远程调用方法，可实现当前市面所有的功能需求；
8. 框架内实现了一个标准的周期任务，周期单位为S，可应用于应用的周期检测任务，用户可自定义是否启用，自定义周期任务类型；
9. 框架内集成了完备的日志管理系统，可实现日志多种输出、自定义等级输出、日志自动转存，可限定日志大小等功能，非常适合嵌入式设备使用；
10. 框架本身集成了异常监控功能，结合日志系统，可将应用程序的各种异常实时记录，非常便于应用BUG的记录、跟踪、定位和解决；
11. 框架自身具备配置文件监控功能，当配置文件有修改时，可自动重载配置文件，从而实现动态部署、动态修改日志输出方法；
12. 框架本身提供了相关的一类工具集接口，可用于获取ERPC版本、运行状态、当前进程名、cJSON复制、字符串比较等等功能，并且还在不断扩展；
13. 整个框架只需要**1个环境变量**：ERPC_PROFILE_PATH；和**2个配置文件**：部署配置文件、日志配置文件；所有框架接口仅仅**只有11个接口**：
```
/* 框架控制接口：3个 */
typedef enum {
    ERPC_LOOP_EXIT,
    ERPC_LOOP_DEFAULT,
    ERPC_LOOP_ONCE,
    ERPC_LOOP_NOWAIT
}erpc_loop_t;

extern int erpc_framework_init(char *process);
extern int erpc_framework_loop(erpc_loop_t way);
extern void erpc_framework_break(void);

/* 远程调用接口：3个 */
typedef cJSON *(*erpc_service_callback_t)(cJSON *params);

extern int erpc_service_register(const char *module, const char *func, erpc_service_callback_t pointer);
extern int erpc_service_unregister(const char *module, const char *service);
extern int erpc_service_proxy_call(const char *module, const char *service, cJSON *send, cJSON **recv, struct timeval *tv);

/* 观察者模式接口：5个 */
typedef void (*erpc_observer_callback_t)(cJSON *params);

extern int erpc_observed_create(const char *module, const char *observed);
extern int erpc_observed_destroy(const char *module, const char *observed);
extern int erpc_observer_invoke(const char *module, const char *observed, cJSON *params);
extern int erpc_observer_register(const char *module, const char *observed, erpc_observer_callback_t action, struct timeval *tv);
extern int erpc_observer_unregister(const char *module, const char *observed, erpc_observer_callback_t action, struct timeval *tv);
```

- 想快速上手，熟悉其使用方法，详见：[快速入门](./2.QuickStart.md)；
- 有关框架及接口的详细介绍，详见：[使用手册](./3.UserGuide.md)；
- 有关配置文件的详细介绍，详见：[配置文件](./4.Profile.md);
- 有关日志系统的详细介绍，详见：[日志系统](./5.Logger.md)

除以上框架本身的功能之外，框架还集成了如下模块，同时具备集成模块的全部功能：
1. 集成cJSON库，ERPC内部也使用该库处理JSON数据，详见[cJSON](../Peripheral/1.cJSON.md)；
2. 集成libuv事件库，可用于实现高性能事件驱动的应用程序；
3. 集成libevent通信库，可用于实现高性能事件驱动的应用程序；
