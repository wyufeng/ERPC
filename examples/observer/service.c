#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "erpc.h"

cJSON *hello_world_service(cJSON *params)
{
    char *message = NULL;
    cJSON *result = NULL;
    cJSON *status = NULL;

    /* deal with params : print params */
    message = cJSON_Print(params);
    printf("params: %s\n", message);
    free(message);
    cJSON_Delete(params);

    /* invoke status */
    status = cJSON_CreateObject();
    cJSON_AddStringToObject(status, "status", "hello");
    erpc_observer_invoke("hello", "status", status);

    /* response remote */
    result = cJSON_CreateObject();
    cJSON_AddStringToObject(result, "hello", "I'm helloworld service.");

    return result;
}


void hello_status_handler(cJSON *params)
{
    char *message = NULL;
    message = cJSON_Print(params);
    printf("status params: %s\n", message);
    free(message);
    cJSON_Delete(params);
}

int main(void)
{
    erpc_framework_init("service");

    erpc_observed_create("hello", "status");

    erpc_service_register("hello", "helloworld", hello_world_service);

    erpc_observer_register("hello", "status", hello_status_handler, NULL);

    return erpc_framework_loop(ERPC_LOOP_DEFAULT);
}

